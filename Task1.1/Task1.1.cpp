﻿#include <iostream>
#include "windows.h"
typedef int elemtype;

struct elem {
	elemtype value;
	struct elem* next;
	struct elem* prev;
};

struct myList {
	struct elem* head;
	struct elem* tail;
	size_t size;
};

typedef struct elem cNode;
typedef struct myList cList;
//Створення списку
cList* createList() {
	cList* list = (cList*)malloc(sizeof(cList));
	if (list) {
		list->size = 0;
		list->head = list->tail = NULL;
	}
	return list;
}
//Видалення списку
void deleteList(cList* list) {
	cNode * head = list->head;
	cNode* next = NULL;
	while (head) {
		next = head->next;
		free(head);
		head = next;
	}
	free(list);
	list = NULL;
}
//Перевірка на пустоту списку
bool isEmptyList(cList* list) {
	return ((list->head == NULL) || (list->tail == NULL));
}
//Вставка елементу на початок
int pushFront(cList* list, elemtype* data) {
	cNode* node = (cNode*)malloc(sizeof(cNode));
	if (!node) {
		return(-1);
	}
	node->value = *data;
	node->next = list->head;
	node->prev = NULL;
	if (!isEmptyList(list)) {
		list->head->prev = node;
	}
	else {
		list->tail = node;
	}
	list->head = node;
	list->size++;
	return(0);
}
//Вилучення елементу з початку
int popFront(cList* list, elemtype* data) {
	cNode* node;
	if (isEmptyList(list)) {
		return(-2);
	}
	node = list->head;
	list->head = list->head->next;
	if (!isEmptyList(list)) {
		list->head->prev = NULL;
	}
	else {
		list->tail = NULL;
	}
	*data = node->value;
	list->size--;
	free(node);
	return(0);
}
//Вставка елементу в кінець
int pushBack(cList* list, elemtype* data) {
	cNode* node = (cNode*)malloc(sizeof(cNode));
	if (!node) {
		return(-3);
	}
	node->value = *data;
	node->next = NULL;
	node->prev = list->tail;
	if (!isEmptyList(list)) {
		list->tail->next = node;
	}
	else {
		list->head = node;
	}
	list->tail = node;
	list->size++;
	return(0);
}
//Вилучення елемента з кінця
int popBack(cList* list, elemtype* data) {
	cNode* node = NULL;
	if (isEmptyList(list)) {
		return(-4);
	}
	node = list->tail;
	list->tail = list->tail->prev;
	if (!isEmptyList(list)) {
		list->tail->next = NULL;
	}
	else {
		list->head = NULL;
	}
	*data = node->value;
	list->size--;
	free(node);
	return(0);
}
//Пошук по індексу
cNode* getNode(cList* list, int index) {
	cNode* node = NULL;
	int i;
	if (index >= list->size) {
		return (NULL);
	}
	if (index < list->size / 2) {
		i = 0;
		node = list->head;
		while (node && i < index) {
			node = node->next;
			i++;
		}
	}
	else {
		i = list->size - 1;
		node = list->tail;
		while (node && i > index) {
			node = node->prev;
			i--;
		}
	}
	return node;
}
//Вставка елемента в середину
int insert(cList* list, int index, elemtype* value) {
	cNode* elm = NULL;
	cNode * ins = NULL;
	elm = getNode(list, index);
	if (elm == NULL) {
		return (-5);
	}
	ins = (cNode*)malloc(sizeof(cNode));
	ins->value = *value;
	ins->prev = elm;
	ins->next = elm->next;
	if (elm->next) {
		elm->next->prev = ins;
	}
	elm->next = ins;
	if (!elm->prev) {
		list->head = elm;
	}
	if (!elm->next) {
		list->tail = elm;
	}
	list->size++;
	return 0;
}
//Видалення елемента з середини
int deleteNode(cList* list, int index, elemtype* data) {
	cNode* elm = NULL;
	elemtype tmp = NULL;
	elm = getNode(list, index);
	if (elm == NULL) {
		return (-6);
	}
	if (elm->prev) {
		elm->prev->next = elm->next;
	}
	if (elm->next) {
		elm->next->prev = elm->prev;
	}
	tmp = elm->value;
	if (!elm->prev) {
		list->head = elm->next;
	}
	if (!elm->next) {
		list->tail = elm->prev;
	}
	*data = elm->value;
	free(elm);
	list->size--;
	return 0;
}

void printList(cList* list, void (*func)(elemtype*)) {
	cNode* node = list->head;
	if (isEmptyList(list)) {
		return;
	}
	while (node) {
		func(&node->value);
		node = node->next;
	}
	
}

void printNode(elemtype* value) {
	printf("%d\n", *((int*)value));
}

int main() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	cList* mylist = createList();
	elemtype tmp;
	int task = 0;
	int index;
	do {
		printf("Що потрібно зробить?\n");
		printf("1) Додати елемент на початок списку\n");
		printf("2) Додати елемент на кінець списку\n");
		printf("3) Додати елемент після вказаного індексу\n");
		printf("4) Видалити перший елемент списку\n");
		printf("5) Видалити останній елемент списку\n");
		printf("6) Видалити елемент з вказаним індексом\n");
		printf("7) Введіть окремий елемент\n");
		printf("8) Вивести весь список\n");
		printf("9) Видалити список\n");
		printf("0) Вийти\n");
		scanf_s("%d", &task);
		elemtype elem;
		switch (task) {
		case 1:
			printf("\n");
			printf("Введіть значення елемента\n");
			scanf_s("%d", &elem);
			pushFront(mylist, &elem);
			printf("\n");
			break;
		case 2:
			printf("\n");
			printf("Введіть значення елемента\n");
			scanf_s("%d", &elem);
			pushBack(mylist, &elem);
			printf("\n");
			break;
		case 3:
			printf("\n");
			printf("Введіть значення елемента\n");
			scanf_s("%d", &elem);
			printf("Введіть індекс, після якого вставити елемент\n");
			scanf_s("%d", &index);
			insert(mylist, index, &elem);
			printf("\n");
			break;
		case 4:
			printf("\n");
			popFront(mylist, &tmp);
			printf("Елемент видалений");
			printf("\n");
			break;
		case 5:
			printf("\n");
			popBack(mylist, &tmp);
			printf("Елемент видалений");
			printf("\n");
			break;
		case 6:
			printf("\n");
			printf("Введіть індекс, який потрібно видалити\n");
			scanf_s("%d", &index);
			deleteNode(mylist, index, &tmp);
			printf("\n");
			break;
		case 7:
			printf("\n");
			printf("Введіть індекс, який потрібно вивести\n");
			scanf_s("%d", &index);
			printNode(&getNode(mylist, index)->value);
			printf("\n");
			break;
		case 8:
			printf("\n");
			printList(mylist, printNode);
			printf("\n");
			break;
		case 9:
			deleteList(mylist);
			break;
		}
		system("pause");
		system("cls");
	} while (task);
	return 0;
}

